# Bubbles Application User Guide

![bubbles.png](bubbles.png)

Welcome to Bubbles, the family relationship management application. This documentation will guide you through the main features of the application, allowing you to add and manage your family members and their relationships.

## Prerequisites
- Java 21
- JavaFX 21

## Installation
1. Clone the GitLab repository: git clone https://gitlab.com/romain.trimouille/s-6-a-01.git
2. Open your preferred IDE and launch the project.

## Adding a Member
To add a new member to the family, follow these steps:

1. Click on the Add Family Member button in the toolbar.
2. Click on the location where you want to add your member.
3. Fill in the required information in the form (name and image).
4. Click the Ok button to add the member to the family.
5. A new member will appear in your family.

## Modifying a Member
To modify the name or image of an existing member:

1. Click on the Update Family Member button.
2. Select the member you want to modify by clicking on their image.
3. Make the desired changes in the form.
4. Click Ok to apply the changes.

## Adding a Relationship
To add a relationship between two members:

1. Click on the Add Relation button.
2. Select the first member by clicking on their image, then select the second member to link.
3. A relationship will be added between the two members.

## Deleting a Relationship
To delete an existing relationship between two members:

1. Select one of the members involved in the relationship by clicking on their name.
2. Click on the Delete Relation button.
3. Click on the two members whose relationship you want to delete.
4. The relationship between the two members will disappear.

## Finding the Shortest Path between Two Members
To find the shortest path of relationships between two members:

1. Click on the Find shortest path button in the toolbar.
2. Select the starting member and the target member.
3. A popup will appear showing you the shortest path between the two members.

## Documentation of Used Technologies
1. Programming Language Used
- Java 21
Description: Java is an object-oriented programming language widely used for developing robust, secure, and high-performance software.

2. Graphic Library
- JavaFX 21
Description: JavaFX is a graphic library for Java, used to develop feature-rich applications with modern user interfaces. Version 21 of JavaFX brings significant updates in terms of performance and features.

3. Testing and Benchmarking Tools
- JUnit 5
Description: JUnit is a testing framework for Java that allows the creation and execution of unit tests. Version 5 of JUnit (also known as JUnit Jupiter) introduces new features and improvements to facilitate writing tests.

- JMH (Java Microbenchmark Harness)
Description: JMH is a tool developed by the OpenJDK team to facilitate writing microbenchmarks in Java. It is designed to precisely measure the performance of code segments.

4. Collaboration and Version Control Tools
- Git
Description: Git is a decentralized version control system used to track changes made to source code over time. It enables effective collaboration among multiple developers.

- GitLab
Description: GitLab is a Git repository management platform that offers CI/CD features, project management, and collaboration tools. It is used to host the source code and manage the issues of the "Bubbles" project.
