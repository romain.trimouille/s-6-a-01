package fr.iut.rodez.bubbles.controller;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.fx.commons.Paints;
import fr.iut.rodez.bubbles.fx.components.Dialogs;
import fr.iut.rodez.bubbles.fx.view.FamilyView;
import fr.iut.rodez.bubbles.fx.view.MainView;
import fr.iut.rodez.bubbles.service.FamilyService;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.Optional;

public final class MainController implements Controller {

    private Stage stage;
    private MainView view;

    public MainController(Stage stage) {
        this.stage = stage;
    }

    public void createFamily() {
        Dialogs.askDialog("Please enter the family name").ifPresent(name -> {
           try {
               Family family = new Family(name);
               switchToFamilyView(family);
           } catch (IllegalArgumentException exception) {
               Dialogs.errorDialog("Please enter a valid family name.");
           }
       });
    }

    public void loadFamilyFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Family File...");
        File file = fileChooser.showOpenDialog(view.getStage());
        if (file != null) {
            switch (FamilyService.loadFamilyFromFile(file)) {
                case FamilyService.FamilyLoadingResult.Loaded loaded -> switchToFamilyView(loaded.family());
                case FamilyService.FamilyLoadingResult.LoadingError loadingError -> onLoadingError(loadingError.cause());
            }
        }
    }

    public void closeApp() {
        if (Dialogs.confirmDialog("Do you really want to close app ?")) {
            view.getStage().close();
        }
    }

    private void switchToFamilyView(Family family) {
        FamilyController familyController = new FamilyController(view.getStage(), family);
        familyController.displayView();
    }

    private void onLoadingError(Exception cause) {
        System.out.println(cause.getMessage());
    }

    @Override
    public void displayView() {
        this.view = new MainView(stage);
        view.setController(this);

        Scene scene = new Scene(view, 400, 300);

        scene.setFill(Paints.BACKGROUND);
        view.getStage().setScene(scene);
        view.getStage().show();
    }
}
