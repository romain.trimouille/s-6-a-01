package fr.iut.rodez.bubbles.controller;

public interface Controller {
    void displayView();
}
