package fr.iut.rodez.bubbles.controller;

import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.commons.Paints;
import fr.iut.rodez.bubbles.fx.components.Dialogs;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import fr.iut.rodez.bubbles.fx.view.FamilyView;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.UUID;

public class FamilyController implements Controller {

    private final Family family;
    private FamilyView view;
    private FamilyMember selectedMember;
    private Stage stage;

    public FamilyController(Stage stage, Family family) {
        this.stage = stage;
        this.family = family;
    }

    public Optional<FamilyMember> createMember(String name, URL image, Position position) {
        Identity identity = new Identity(UUID.randomUUID(), name, image);
        FamilyMember member = null;

        try {
            member = new FamilyMember(identity, position);
            family.addMember(member);
        } catch (IllegalArgumentException illegalArgumentException) {
            Dialogs.errorDialog("Please enter a valid member name.");
        }
        return Optional.ofNullable(member);
    }



    @Override
    public void displayView() {
        view = new FamilyView(stage);
        view.setController(this);

        Scene scene = new Scene(view);

        scene.setFill(Paints.BACKGROUND);
        view.getStage().setScene(scene);
        view.getStage().setMaximized(true);
        view.getStage().show();
    }
}