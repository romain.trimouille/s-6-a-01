package fr.iut.rodez.bubbles;

import fr.iut.rodez.bubbles.controller.MainController;
import fr.iut.rodez.bubbles.fx.commons.Images;
import fr.iut.rodez.bubbles.fx.view.MainView;
import javafx.application.Application;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("My Family");
        primaryStage.getIcons().add(new ImageView(Images.LOGO).getImage());

        MainController mainController = new MainController(primaryStage);
        mainController.displayView();
    }

    public static void main(String[] args) {
        launch(args);
    }
}