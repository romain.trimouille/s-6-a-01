package fr.iut.rodez.bubbles.domain.relation;

import java.util.UUID;

public class FamilyRelation {

    private final UUID source;
    private final UUID target;
    private final RelationType type;

    public FamilyRelation(UUID source, UUID target, RelationType type) {
        this.source = source;
        this.target = target;
        this.type = type;
    }

    public UUID getSource() {
        return source;
    }

    public UUID getTarget() {
        return target;
    }

    public RelationType getType() {
        return type;
    }
}