package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.utils.Validations;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyMember implements Social<FamilyMember> {

    public final Identity identity;

    private Position position;

    private final HashSet<Relation> relations;

    public FamilyMember(Identity identity, Position position) {
        this.identity = Objects.requireNonNull(identity, "Identity must not be null.");
        this.position = Objects.requireNonNull(position, "Position must not be null.");
        this.relations = new HashSet<>();
    }

    public void addRelation(Relation relation) {
        relations.add(relation);
    }

    public void removeRelation(Relation relation) {
        relations.remove(relation);
    }

    public void removeAllRelations() {
        relations.clear();
    }
    
    public Set<Relation> relations() {
        return Set.copyOf(relations);
    }

    public Set<FamilyMember> lovedOnes() {
        return lovedOnesAsStream().collect(Collectors.toSet());
    }

    public Set<FamilyMember> children() {
        return relations
                .stream()
                .filter(ChildRelation.class::isInstance)
                .map(Relation::related)
                .collect(Collectors.toSet());
    }

    private Stream<FamilyMember> lovedOnesAsStream() {
        return relations
                .stream()
                .map(Relation::related);
    }

    public Set<FamilyMember> relatives(int maxDegree) {
        Validations.requireStrictlyPositive(maxDegree, () -> "Max degree must be at least 1.");

        Set<FamilyMember> relatives = new HashSet<>();
        Collection<FamilyMember> familyMembersAtNextDegree = List.of(this);

        for (int degree = 1; degree <= maxDegree; degree++) {

            familyMembersAtNextDegree = familyMembersAtNextDegree
                    .stream()
                    .flatMap(FamilyMember::lovedOnesAsStream)
                    .filter(familyMember -> familyMember != this && !relatives.contains(familyMember))
                    .toList();

            relatives.addAll(familyMembersAtNextDegree);
        }

        return relatives;
    }

    public Position currentPosition() {
        return position;
    }

    public void moveTo(Position position) {
        this.position = position;
    }

    @Override
    public void parentOf(FamilyMember other) {
        relations.add(new ChildRelation(other));
        other.relations.add(new ParentRelation(this));
    }
}