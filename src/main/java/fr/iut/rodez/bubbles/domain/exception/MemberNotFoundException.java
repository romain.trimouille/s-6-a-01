package fr.iut.rodez.bubbles.domain.exception;

import fr.iut.rodez.bubbles.domain.Family;

import java.util.UUID;

public class MemberNotFoundException extends FamilyException {

    private static final String templateMessage = "Member '%s' not found in family '%s'.";

    public MemberNotFoundException(UUID uuid, Family family) {
        super(String.format(templateMessage, uuid.toString(), family.getName()));
    }
}
