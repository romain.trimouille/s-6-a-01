package fr.iut.rodez.bubbles.domain.exception;

import fr.iut.rodez.bubbles.domain.Family;

import java.util.UUID;

public class MembersAlreadyRelatedException extends FamilyException {

    private static final String templateMessage = "Members '%s' and '%s' are already related in family '%s'.";

    public MembersAlreadyRelatedException(UUID fromUuid, UUID toUuid, Family family) {
        super(String.format(templateMessage, fromUuid.toString(), toUuid.toString(), family.getName()));
    }
}
