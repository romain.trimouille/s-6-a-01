package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.domain.exception.FamilyException;
import fr.iut.rodez.bubbles.domain.exception.MemberNotFoundException;
import fr.iut.rodez.bubbles.domain.exception.MembersAlreadyRelatedException;
import fr.iut.rodez.bubbles.domain.relation.FamilyRelation;
import fr.iut.rodez.bubbles.utils.Validations;
import javafx.beans.value.ObservableValue;

import java.util.*;
import java.util.stream.Collectors;

public class Family {
    private final String name;
    private final Set<FamilyMember> members;

    private final Set<FamilyRelation> relations;

    public Family(String name) {
        this(name, new HashSet<>());
    }

    public Family(String name, Set<FamilyMember> members) {
        this.name = Validations.requireNotBlank(name, () -> "Name must not be null.");
        this.members = Objects.requireNonNull(members, "Members must not be null.");
        this.relations = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public Set<FamilyMember> getMembers() {
        return Set.copyOf(members); // defensive copy
    }

    public void addMember(FamilyMember member) {
        members.add(member);
        System.out.println("Member added");
    }

    public void removeMember(UUID memberToRemove) {
        relations.removeIf(relation -> relation.getSource() == memberToRemove || relation.getTarget() == memberToRemove);
        members.remove(memberToRemove);
    }



    public Set<FamilyRelation> getRelations() {
        return Set.copyOf(relations); // defensive copy
    }

    /**
     * Add a relation between two members of the family.
     * @param relation The relation to add
     * @throws MemberNotFoundException if a member of the relation does not exist in family
     * @throws MembersAlreadyRelatedException If a relation already exists between two members
     */
    public void addRelation(FamilyRelation relation) throws FamilyException {
        final UUID source = relation.getSource();
        final UUID target = relation.getTarget();

        if (! findMemberById(source).isPresent())
            throw new MemberNotFoundException(relation.getTarget(), this);

        if (! findMemberById(target).isPresent())
            throw new MemberNotFoundException(relation.getTarget(), this);

        if (source.equals(target))
            throw new FamilyException("Member cannot be related himself");

        if (findRelationBetween(relation.getSource(), relation.getTarget()).isPresent()) {
            throw new MembersAlreadyRelatedException(source, target, this);
        }

        relations.add(relation);
    }

    public void removeRelation(FamilyRelation relation) {
        relations.remove(relation);
    }

    public Optional<FamilyRelation> findRelationBetween(UUID member1, UUID member2) {
        return relations.stream()
                .filter(relation -> (
                        relation.getSource() == member1 && relation.getTarget() == member2)
                        || (relation.getTarget() == member1 && relation.getSource() == member2))
                .findFirst();
    }

    public Optional<FamilyMember> findMemberById(UUID memberId) {
        return members.stream()
                .filter(m -> m.identity.id().equals(memberId))
                .findFirst();
    }
}
