package fr.iut.rodez.bubbles.domain.exception;

public class FamilyException extends Exception {

    public FamilyException(String message) {
        super(message);
    }
}
