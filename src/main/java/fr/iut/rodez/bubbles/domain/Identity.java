package fr.iut.rodez.bubbles.domain;

import java.net.URL;
import java.util.UUID;

public record Identity(
        UUID id,
        String name,
        URL picture
) {



}
