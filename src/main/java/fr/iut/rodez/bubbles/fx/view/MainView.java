package fr.iut.rodez.bubbles.fx.view;

import fr.iut.rodez.bubbles.controller.Controller;
import fr.iut.rodez.bubbles.controller.MainController;
import fr.iut.rodez.bubbles.utils.StaticResources;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainView extends View {

    private final Button newButton;
    private final Button loadButton;
    private final Button closeButton;
    private MainController controller;

    public MainView(Stage stage) {
        super(stage);

        Label title = new Label("My Family");
        newButton = new Button("Create");
        loadButton = new Button("Load");
        closeButton = new Button("Exit");

        VBox menu = new VBox(10);
        menu.getStyleClass().add("menu");
        menu.setPadding(new Insets(10));
        menu.getChildren().addAll(title, newButton, loadButton, closeButton);

        setCenter(menu);
        getStylesheets().add(StaticResources.retrieveResourceURL("/styles.css").toExternalForm());
        setupListeners();
    }

    public void setupListeners() {
        newButton.setOnAction(event -> controller.createFamily());
        loadButton.setOnAction(event -> controller.loadFamilyFile());
        closeButton.setOnAction(event -> controller.closeApp());
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public Button getNewButton() {
        return newButton;
    }

    public Button getLoadButton() {
        return loadButton;
    }

    public Button getCloseButton() {
        return closeButton;
    }
}
