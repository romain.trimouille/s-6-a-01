package fr.iut.rodez.bubbles.fx.components.actions;

import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import javafx.scene.control.ToggleGroup;

import java.util.function.Consumer;

public class ActionsGroup extends ToggleGroup {

    private Consumer<FamilyGraphResizableCanvas.EventManagementStrategy> onEventManagementStrategyChange;

    public ActionsGroup(ActionButton[] actionButtons) {
        for (ActionButton actionButton : actionButtons) {
            actionButton.setToggleGroup(this);
        }

        selectedToggleProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue instanceof ActionButton) {
                        onEventManagementStrategyChange.accept(((ActionButton) newValue).getStrategy());
                    } else {
                        selectToggle(oldValue);
                    }
                });
    }

    public void setOnEventManagementStrategyChange(Consumer<FamilyGraphResizableCanvas.EventManagementStrategy> onEventManagementStrategyChange) {
        this.onEventManagementStrategyChange = onEventManagementStrategyChange;
    }

}
