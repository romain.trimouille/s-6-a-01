package fr.iut.rodez.bubbles.fx.components.actions;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class MemberActionBox extends HBox {

    private Label label;
    private Button updateButton;
    private Button removeButton;

    public MemberActionBox(FamilyMember member) {
        label = new Label(member.identity.name());

        updateButton = new Button("Update");
        removeButton = new Button("Delete");
    }

    public void setUpdateButtonAction(EventHandler<ActionEvent> handler) {
        updateButton.setOnAction(handler);
    }

    public void setRemoveButtonAction(EventHandler<ActionEvent> handler) {
        removeButton.setOnAction(handler);
    }
}
