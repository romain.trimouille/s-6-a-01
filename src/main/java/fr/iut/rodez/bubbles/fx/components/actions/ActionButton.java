package fr.iut.rodez.bubbles.fx.components.actions;

import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

public class ActionButton extends ToggleButton {

    private final FamilyGraphResizableCanvas.EventManagementStrategy strategy;

    public ActionButton(String text, Image image, FamilyGraphResizableCanvas.EventManagementStrategy strategy) {
        super(text, new ImageView(image));
        this.strategy = strategy;

        getStyleClass().add("action-button");
    }

    public FamilyGraphResizableCanvas.EventManagementStrategy getStrategy() {
        return strategy;
    }
}
