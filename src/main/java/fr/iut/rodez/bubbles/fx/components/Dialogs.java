package fr.iut.rodez.bubbles.fx.components;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.Optional;

public class Dialogs extends TextInputDialog {

    public static Optional<String> askDialog(String message) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Ask");
        dialog.setHeaderText(message);

        return dialog.showAndWait();
    }

    public static Optional<String> askDialog(String message, String contentText) {
        TextInputDialog dialog = new TextInputDialog(contentText);
        dialog.setTitle("Ask");
        dialog.setHeaderText(message);

        return dialog.showAndWait();
    }

    public static void informDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static boolean confirmDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(message);
        Optional<ButtonType> result = alert.showAndWait();

        return result.isPresent() && result.get() == ButtonType.OK;
    }

    public static void errorDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    public static Optional<File> fileChooserDialog(String message) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg"));
        return Optional.ofNullable(fileChooser.showOpenDialog(null));
    }



}
