package fr.iut.rodez.bubbles.fx.graphics;

import fr.iut.rodez.bubbles.domain.Identity;

public interface Draggable extends Drawable, Mobile {


    void updateIdentity(Identity newIdentity);
}
