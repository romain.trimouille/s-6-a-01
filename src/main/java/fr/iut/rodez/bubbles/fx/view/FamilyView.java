package fr.iut.rodez.bubbles.fx.view;

import fr.iut.rodez.bubbles.controller.Controller;
import fr.iut.rodez.bubbles.controller.FamilyController;
import fr.iut.rodez.bubbles.controller.MainController;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.components.Dialogs;
import fr.iut.rodez.bubbles.fx.components.actions.GlobalActionBox;
import fr.iut.rodez.bubbles.fx.components.relations.RelationVerticalList;
import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.utils.StaticResources;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.NODE_MANIPULATION;

public class FamilyView extends View {

    private final FamilyGraphResizableCanvas graph;
    private FamilyController controller;

    public FamilyView(Stage stage) {
        super(stage);
        Family family = new Family("test"); // TODO modifier;

        getStylesheets().add(StaticResources.retrieveResourceURL("/styles.css").toExternalForm());

        setPadding(new Insets(8));
        setBackground(Backgrounds.TRANSPARENT);

        Pane center = new Pane();
        GraphicalFamily graphicalFamily = new GraphicalFamily(family);

        GlobalActionBox globalActionBox = new GlobalActionBox();

        graph = new FamilyGraphResizableCanvas(graphicalFamily, NODE_MANIPULATION, globalActionBox);

        graph.widthProperty().bind(center.widthProperty());
        graph.heightProperty().bind(center.heightProperty());
        center.getChildren().add(graph);

        globalActionBox.getActionsGroup().setOnEventManagementStrategyChange(graph::setEventManagementStrategy);

        setRight(createContainerFor(new RelationVerticalList(graphicalFamily)));
        setCenter(createContainerFor(center));
        setBottom(createContainerFor(globalActionBox));
    }

    public void setupListeners() {
        //graph.setOnAction(event -> controller.createMember());
    }

    public void setupListener() {

    }

    public void setController(FamilyController controller) {
        this.controller = controller;
    }

    public FamilyController getController() {
        return controller;
    }

    public Region createContainerFor(Region region) {
        BorderPane pane = new BorderPane(region);
        pane.setPadding(new Insets(8));
        return pane;
    }

    public void onClickCreateMember(Position position) {
        Optional<String> name = Dialogs.askDialog("Please enter the member name");
        Optional<File> image = Dialogs.fileChooserDialog("Select picture");

        if (name.isPresent() && image.isPresent()) {
            try {
                URL imageUrl = image.get().toURI().toURL();
                Optional<FamilyMember> member = controller.createMember(name.get(), imageUrl, position);
                member.ifPresent(graph::addMemberToGraph);
            } catch (MalformedURLException malformedURLException) {
                Dialogs.errorDialog("Could not load the file as a URL.");
            }
        }
    }
}