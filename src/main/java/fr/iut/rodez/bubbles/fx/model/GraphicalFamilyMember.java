package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.domain.Social;
import fr.iut.rodez.bubbles.fx.graphics.Draggable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class GraphicalFamilyMember implements Draggable, Social<GraphicalFamilyMember> {

    private static final double CIRCLE_RADIUS = 32.0;
    private FamilyMember familyMember;
    private List<GraphicalFamilyMember> relations = new ArrayList<>();
    private boolean isSelected;


    public List<GraphicalFamilyMember> getRelations() {
        return relations;
    }

    public void addRelation(GraphicalFamilyMember other) {
        if (!relations.contains(other)) {
            relations.add(other);
            other.relations.add(this);
        }
    }

    public GraphicalFamilyMember(FamilyMember familyMember) {
        this.familyMember = familyMember;
        this.isSelected = false;
    }

    public Position currentPosition() {
        return familyMember.currentPosition();
    }

    public Identity identity() {
        return familyMember.identity;
    }

    @Override
    public void drawWith(GraphicsContext context) {
        Position pos = currentPosition();
        double x = pos.x() - CIRCLE_RADIUS;
        double y = pos.y() - CIRCLE_RADIUS;
        double diameter = CIRCLE_RADIUS * 2;

        context.fillOval(x, y, diameter, diameter); // Draw the filled circle

        if (isSelected) { // Check if the member is selected
            context.setStroke(Color.BLUE);
            context.setLineWidth(2);
            context.strokeOval(x, y, diameter, diameter); // Draw blue border
        }
    }

    public void removeRelation(GraphicalFamilyMember other) {
        relations.remove(other);
        other.relations.remove(this);
    }

    @Override
    public boolean covers(Position position) {
        return currentPosition().distanceTo(position) <= CIRCLE_RADIUS;
    }

    @Override
    public void moveTo(Position position) {
        familyMember.moveTo(position);
    }

    @Override
    public void parentOf(GraphicalFamilyMember other) {
        familyMember.parentOf(other.familyMember);
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    
    @Override
    public void updateIdentity(Identity newIdentity) {
        this.familyMember = new FamilyMember(newIdentity, familyMember.currentPosition());
    }
}
