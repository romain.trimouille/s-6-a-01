package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.Relation;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class GraphicalFamily {

    public final ListProperty<GraphicalFamilyMember> members;
    public final ListProperty<GraphicalRelation> relations;

    public GraphicalFamily(Family family) {

        Map<UUID, GraphicalFamilyMember> membersById = family.getMembers()
                                                             .stream()
                                                             .collect(toMap(f -> f.identity.id(), GraphicalFamilyMember::new));

        this.members = new SimpleListProperty<>(FXCollections.observableArrayList(membersById.values()));
        this.relations = new SimpleListProperty<>(FXCollections.observableArrayList());

        family.getMembers()
              .forEach(member -> {
                  Set<Relation> relations = member.relations();
                  relations.forEach(relation -> {
                      GraphicalFamilyMember source = membersById.get(member.identity.id());
                      GraphicalFamilyMember target = membersById.get(relation.related().identity.id());
                      createRelation(source, target);
                  });
              });
    }

    public void createRelation(GraphicalFamilyMember source, GraphicalFamilyMember target) {
        relations.add(new GraphicalRelation(source, target));
        source.addRelation(target);
        target.addRelation(source);
    }


    public void deleteRelation(GraphicalFamilyMember source, GraphicalFamilyMember target) {
        relations.removeIf(relation -> relation.source().equals(source) && relation.target().equals(target));
        relations.removeIf(relation -> relation.target().equals(source) && relation.source().equals(target));

        source.removeRelation(target);
        target.removeRelation(source);
        showAlert("Deletion Successful", "The relation has been successfully deleted.");




    }


    private void showAlert(String title, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION); // Changed to INFORMATION for non-error messages
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.showAndWait();
    }




    public List<GraphicalFamilyMember> findShortestPathDijkstra(UUID startId, UUID endId) {
        Map<UUID, GraphicalFamilyMember> memberMap = members.stream()
                .collect(Collectors.toMap(member -> member.identity().id(), member -> member));

        Map<UUID, Double> distances = new HashMap<>();
        Map<UUID, GraphicalFamilyMember> predecessors = new HashMap<>();
        PriorityQueue<GraphicalFamilyMember> priorityQueue = new PriorityQueue<>(
                Comparator.comparingDouble(member -> Optional.ofNullable(distances.get(member.identity().id())).orElse(Double.MAX_VALUE))
        );

        members.forEach(member -> distances.put(member.identity().id(), Double.MAX_VALUE));
        distances.put(startId, 0.0);

        priorityQueue.add(memberMap.get(startId));

         while (!priorityQueue.isEmpty()) {
            GraphicalFamilyMember current = priorityQueue.poll();
            if (current.identity().id().equals(endId)) {
                return reconstructPath(predecessors, memberMap.get(endId));
            }

            current.getRelations().forEach(neighbor -> {
                double newDist = distances.get(current.identity().id()) + 1;
                if (newDist < distances.get(neighbor.identity().id())) {
                    distances.put(neighbor.identity().id(), newDist);
                    predecessors.put(neighbor.identity().id(), current);
                    priorityQueue.add(neighbor);
                }
            });
        }

        return Collections.emptyList(); // No path found
    }

    private List<GraphicalFamilyMember> reconstructPath(Map<UUID, GraphicalFamilyMember> predecessors, GraphicalFamilyMember endMember) {
        LinkedList<GraphicalFamilyMember> path = new LinkedList<>();
        for (GraphicalFamilyMember at = endMember; at != null; at = predecessors.get(at.identity().id())) {
            path.addFirst(at);
        }
        return path;
    }

    public void deleteMember(GraphicalFamilyMember member) {
        members.remove(member);
        relations.removeIf(rel -> rel.source().equals(member) || rel.target().equals(member));
        showAlert("Deletion Successful", "Member has been successfully deleted.");
    }
}
