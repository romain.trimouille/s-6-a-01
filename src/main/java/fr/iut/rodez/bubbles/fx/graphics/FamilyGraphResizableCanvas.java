package fr.iut.rodez.bubbles.fx.graphics;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.components.Dialogs;
import fr.iut.rodez.bubbles.fx.components.actions.GlobalActionBox;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import fr.iut.rodez.bubbles.fx.view.FamilyView;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.EDGE_MANIPULATION;

public class FamilyGraphResizableCanvas extends ResizableCanvas {

    private GraphicalFamily graphicalFamily;

    private GlobalActionBox globalActionBox;

    private final List<GraphicalFamilyMember> selectedMembers;
    private final GraphicsContext context;
    private EventManagementStrategy eventManagementStrategy;
    private Position point;
    private List<GraphicalFamilyMember> selectedForPath = new ArrayList<>();

    public FamilyGraphResizableCanvas(GraphicalFamily graphicalFamily, EventManagementStrategy eventManagementStrategy, GlobalActionBox globalActionBox) {
        Objects.requireNonNull(graphicalFamily, "family must not be null");
        Objects.requireNonNull(eventManagementStrategy, "event strategy must not be null");

        this.graphicalFamily = graphicalFamily;
        this.eventManagementStrategy = eventManagementStrategy;
        this.selectedMembers = new ArrayList<>();
        this.context = getGraphicsContext2D();
        this.globalActionBox = globalActionBox;

        configureCanvasEvents();
    }

    public void setEventManagementStrategy(EventManagementStrategy eventManagementStrategy) {
        this.eventManagementStrategy = eventManagementStrategy;
        System.out.println("Event Management Strategy set to: " + eventManagementStrategy);
    }

    public void prepareForPathFinding() {
        System.out.println("Pathfinding mode activated.");
        selectedForPath.clear();  // Clear any previously selected members
    }


    public void configureCanvasEvents() {
        setOnMouseDragged(event -> {
            Position position = new Position(event.getX(), event.getY());
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION -> moveSelectedElementsTo(position);
                case EDGE_MANIPULATION -> definePointByReachableElement(position);
            }
            event.consume();
        });

        setOnMousePressed(event -> {
            Position position = new Position(event.getX(), event.getY());
            switch (eventManagementStrategy) {
                //case NODE_MANIPULATION, EDGE_MANIPULATION -> selectFirstElementPositionedOn(position);
                case EDGE_DELETION -> handleEdgeDeletion(position);
                case NODE_MANIPULATION, EDGE_MANIPULATION -> selectFirstElementPositionedOn(position);
            }
            event.consume();
        });

        setOnMouseClicked(event -> {
            Position position = new Position(event.getX(), event.getY());
            switch (eventManagementStrategy) {
                case ADD_MEMBER:
                    ((FamilyView) getScene().getRoot()).onClickCreateMember(position);
                    break;
                case UPDATE_MEMBER:
                    showUpdateMemberDialog(position);
                    break;
                case FIND_PATH:
                    handleFindPathMouseClicked(event);
                    break;
                default:
                    showSelectedMember(position);
                    break;
                case DELETE_MEMBER:
                    selectAndDeleteMember(position);
                    break;
            }
            event.consume();
        });
        setOnMouseReleased(event -> {
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION -> clearSelection();
                case EDGE_MANIPULATION -> selectedAsParentsOfPotentialOne(new Position(event.getX(), event.getY()));
            }
            event.consume();
        });

        setOnMouseExited(event -> {
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION -> clearSelection();
                case EDGE_MANIPULATION -> clearSelectionAndPoint();
            }
            event.consume();
        });
    }

    private void selectAndDeleteMember(Position position) {
        Optional<GraphicalFamilyMember> memberOpt = firstFamilyMemberPositionedOn(position);
        memberOpt.ifPresent(member -> {
            graphicalFamily.deleteMember(member);
            draw();
        });
    }


    private void handleFindPathMouseClicked(MouseEvent event) {
        System.out.println("Mouse clicked for path finding.");

        if (selectedForPath.size() < 2) {
            Position position = new Position(event.getX(), event.getY());
            selectMemberForPath(position);
            System.out.println("Selected for path: " + selectedForPath.size());
            if (selectedForPath.size() == 2) {
                calculateAndDisplayPath();
            }
        } else {
            System.out.println("Resetting path selection.");
            selectedForPath.forEach(member -> member.setSelected(false));
            selectedForPath.clear();
            draw();
        }
    }


    private void selectMemberForPath(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(member -> {
            if (selectedForPath.size() < 2 && !selectedForPath.contains(member)) {
                selectedForPath.add(member);
                member.setSelected(true);  // Set member as selected
                draw();  // Redraw to show selection
                System.out.println("Member selected for path: " + member.identity().name());
            }
            if (selectedForPath.size() == 2) {
                calculateAndDisplayPath();
                resetPathSelectionAfterPathFound(); // Optional: Reset after path is found
            }
        });
    }

    private void resetPathSelectionAfterPathFound() {
        selectedForPath.forEach(member -> member.setSelected(false));
        selectedForPath.clear();
        draw();  // Redraw to clear selection
    }


    private void calculateAndDisplayPath() {
        System.out.println("Calculating path...");
        if (selectedForPath.size() == 2) {
            UUID startId = selectedForPath.get(0).identity().id();
            UUID endId = selectedForPath.get(1).identity().id();
            List<GraphicalFamilyMember> path = graphicalFamily.findShortestPathDijkstra(startId, endId);
            if (path.isEmpty()) {
                showAlert("No Path", "No path could be found between the selected members.");
            } else {
                displayPathInPopup(path);
            }
            resetSelectionAfterPathCalculation();
        }
    }

    private void resetSelectionAfterPathCalculation() {
        selectedForPath.forEach(member -> member.setSelected(false));
        selectedForPath.clear();
        draw();
    }


    private void displayPathInPopup(List<GraphicalFamilyMember> path) {
        System.out.println("Displaying path in popup...");
        if (path.isEmpty()) {
            showAlert("No Path", "No path could be found between the selected members.");
        } else {
            StringBuilder pathDescription = new StringBuilder("Shortest Path:\n");
            for (GraphicalFamilyMember member : path) {
                pathDescription.append(member.identity().name()).append(" -> ");
            }
            pathDescription.setLength(pathDescription.length() - 4); // Remove the last arrow

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Shortest Path Found");
            alert.setHeaderText(null);
            alert.setContentText(pathDescription.toString());
            alert.showAndWait();
        }
    }

    public void addMemberToGraph(FamilyMember member) {
        graphicalFamily.members.add(new GraphicalFamilyMember(member));
        draw();
    }


    private void showUpdateMemberDialog(Position position) {
        selectedMembers.clear();
        firstFamilyMemberPositionedOn(position).ifPresent(selectedMember -> {
            if (selectedMembers.isEmpty()) {
                selectedMembers.add(selectedMember);
            }
        });

        if (selectedMembers.size() == 1) {
            GraphicalFamilyMember member = selectedMembers.get(0);

            Optional<String> result = Dialogs.askDialog("Update the name of the member", member.identity().name());
            result.ifPresent(newName -> {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Select New Image File");
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg"));
                File newFile = fileChooser.showOpenDialog(null);

                if (newFile != null) {
                    try {
                        URL newImageUrl = newFile.toURI().toURL();
                        Identity updatedIdentity = new Identity(member.identity().id(), newName, newImageUrl);
                        member.updateIdentity(updatedIdentity);
                        draw();
                    } catch (MalformedURLException e) {
                        showAlert("File Error", "Could not load the file as a URL.");
                    }
                } else {
                    showAlert("Update Error", "No image selected.");
                }
            });
        } else {
            showAlert("Update Error", "Please select a single member to update.");
        }
    }


    private void showAlert(String title, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.showAndWait();
    }

    private void moveSelectedElementsTo(Position position) {
        if (!selectedMembers.isEmpty()) {
            selectedMembers.forEach(element -> element.moveTo(position));
            draw();
        }
    }

    private void selectFirstElementPositionedOn(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(element -> {
            selectedMembers.clear();
            selectedMembers.add(element);
        });
    }

    private void showSelectedMember(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(element -> {
            graphicalFamily.members.forEach(member -> member.setSelected(false));
            element.setSelected(true);
            selectedMembers.clear();
            selectedMembers.add(element);
            draw();
        });
    }

    private void definePointByReachableElement(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresentOrElse(member -> {
            Position memberPosition = member.currentPosition();
            point = new Position(memberPosition.x(), memberPosition.y());
        }, () -> point = position);
        draw();
    }

    private void selectedAsParentsOfPotentialOne(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(member -> selectedMembers.forEach(other -> graphicalFamily.createRelation(other, member)));
        clearSelectionAndPoint();
    }

    private void clearSelection() {
        selectedMembers.forEach(member -> member.setSelected(false));
        selectedMembers.clear();
        draw();
    }

    private void clearSelectionAndPoint() {
        clearSelection();
        point = null;
    }

    private boolean isTemporaryLineToDraw() {
        return eventManagementStrategy == EDGE_MANIPULATION && point != null && !selectedMembers.isEmpty();
    }

    private Optional<GraphicalFamilyMember> firstFamilyMemberPositionedOn(Position position) {
        return graphicalFamily.members
                .stream()
                .filter(member -> member.covers(position))
                .findFirst();
    }

    @Override
    protected void draw() {
        context.clearRect(0, 0, getWidth(), getHeight());

        graphicalFamily.relations.forEach(relation -> relation.drawWith(context));
        graphicalFamily.members.forEach(member -> member.drawWith(context)); // Redraw each member

        if (isTemporaryLineToDraw() && !selectedMembers.isEmpty()) {
            var firstElement = selectedMembers.getFirst();
            var position = firstElement.currentPosition();
            context.setLineWidth(4.0);
            context.setStroke(Color.GREY);
            context.strokeLine(point.x(), point.y(), position.x(), position.y());
        }

        graphicalFamily.members.forEach(member -> member.drawWith(context));
    }

    private void handleEdgeDeletion(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(selectedMember -> {
            if (selectedMembers.isEmpty()) {
                selectedMembers.add(selectedMember);
            } else if (selectedMembers.contains(selectedMember)) {
                return;
            } else {
                GraphicalFamilyMember firstSelected = selectedMembers.getFirst();
                graphicalFamily.deleteRelation(firstSelected, selectedMember);
                draw();
                selectedMembers.clear();
            }
        });
    }

    public enum EventManagementStrategy {
        NODE_MANIPULATION, EDGE_MANIPULATION, ADD_MEMBER, UPDATE_MEMBER, EDGE_DELETION, FIND_PATH, DELETE_MEMBER
    }
}
