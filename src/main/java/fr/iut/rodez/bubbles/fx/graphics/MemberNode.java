package fr.iut.rodez.bubbles.fx.graphics;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;


public class MemberNode extends Circle {

    private double x;
    private double y;
    private final FamilyMember member;

    public MemberNode(FamilyMember familyMember) {
        this.member = familyMember;

        Image image = new Image(familyMember.identity.picture().toString());
        setFill(new ImagePattern(image));
    }

    public FamilyMember getMember() {
        return member;
    }

    public void setSelected(boolean selected) {
        setStroke(selected ? Color.BLUE : null);
    }
}
