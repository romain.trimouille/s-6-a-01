package fr.iut.rodez.bubbles.fx.components.actions;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.commons.Images;
import javafx.scene.layout.HBox;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.*;

public class GlobalActionBox extends HBox {

    private final ActionButton[] actionButtons = {
            new ActionButton("Persons", Images.PERSON, NODE_MANIPULATION),
            new ActionButton("Add Relation", Images.RELATION, EDGE_MANIPULATION),
            new ActionButton("Delete relation", Images.DELETE_RELATION, EDGE_DELETION),
            new ActionButton("Add Family Member", Images.ADD_MEMBER, ADD_MEMBER),
            new ActionButton("Update Family Member", Images.UPDATE_MEMBER, UPDATE_MEMBER),
            new ActionButton("Find Shortest Path", Images.FIND_SHORTEST, FIND_PATH),
            new ActionButton("Delete Member",Images.DELETE, DELETE_MEMBER),

    };

    private final ActionsGroup actionsGroup;

    public GlobalActionBox() {
        setMaxHeight(64);
        setBackground(Backgrounds.GLASS);

        actionsGroup = new ActionsGroup(actionButtons);
        getChildren().addAll(actionButtons);
    }

    public ActionsGroup getActionsGroup() {
        return actionsGroup;
    }
}
