package fr.iut.rodez.bubbles.fx.commons;

import fr.iut.rodez.bubbles.utils.StaticResources;
import javafx.scene.image.Image;

public final class Images {

    public static final Image LOGO = loadIcon("logo");
    public static final Image PERSON = loadIcon("person");
    public static final Image RELATION = loadIcon("relation");
    public static final Image ADD_MEMBER = loadIcon("addMember");

    public static final Image DELETE_RELATION = loadIcon("deleteRelation");
    public static final Image UPDATE_MEMBER = loadIcon("updateMember");
    public static final Image FIND_SHORTEST = loadIcon("findShortestPath");
    public static final Image DELETE = loadIcon("deleteMember") ;


    private Images() {}

    private static Image loadIcon(String icon) {
        return new Image(StaticResources.loadResourceAsStream("/icons/" + icon + ".png"));
    }
}
