package fr.iut.rodez.bubbles.fx.view;

import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public abstract class View extends BorderPane {
    private final Stage stage;

    public View(Stage stage) {
        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }
}
