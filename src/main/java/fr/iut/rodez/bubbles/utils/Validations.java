package fr.iut.rodez.bubbles.utils;

import java.util.Objects;
import java.util.function.Supplier;

public final class Validations {

    private Validations() {}

    public static String requireNotBlank(String value, Supplier<String> message) {
        Objects.requireNonNull("Value must not ne null");
        if (value.isBlank()) {
            throw new IllegalArgumentException(message.get());
        }
        return value;
    }

    public static String requireNotBlank(String value) {
        return requireNotBlank(value, () -> "Value must no be blank");
    }

    public static double requirePositive(double value, Supplier<String> message) {
        if (Double.isNaN(value) || value < 0.0) {
            throw new IllegalArgumentException(message.get());
        }
        return value;
    }

    public static double requirePositive(double value) {
        return requirePositive(value, () -> "Value must be positive");
    }

    public static int requireStrictlyPositive(int value, Supplier<String> message) {
        if (value <= 0) {
            throw new IllegalArgumentException(message.get());
        }
        return value;
    }

    public static int requireStrictlyPositive(int value) {
        return requireStrictlyPositive(value, () -> "Value must be strictly positive");
    }
}
