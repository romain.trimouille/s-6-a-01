package fr.iut.rodez.bubbles.utils;

import fr.iut.rodez.bubbles.App;

import java.io.InputStream;
import java.net.URL;

public final class StaticResources {

    public static InputStream loadResourceAsStream(String resourcePath) {
        InputStream resourceAsStream = App.class.getResourceAsStream(resourcePath);
        if (resourceAsStream == null) {
            throw new IllegalArgumentException("Resource not found: " + resourcePath);
        }
        return resourceAsStream;
    }

    public static URL retrieveResourceURL(String resourcePath) {
        return App.class.getResource(resourcePath);
    }
}
