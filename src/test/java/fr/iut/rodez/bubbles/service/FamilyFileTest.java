package fr.iut.rodez.bubbles.service;

import fr.iut.rodez.bubbles.domain.Family;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyFileTest {

    @Test
    public void shouldBeAbleToCreateFamilyFile() {
        // Given
        String name = "MyFamilyFile";

        FamilyFile.Member john = new FamilyFile.Member(UUID.randomUUID(), "John", null, new FamilyFile.Member.Position(0, 0));
        FamilyFile.Member bob = new FamilyFile.Member(UUID.randomUUID(), "Bob", null, new FamilyFile.Member.Position(0, 0));
        Set<FamilyFile.Member> members = Set.of(john, bob);

        FamilyFile.Relation johnIsParentOfBob = new FamilyFile.Relation(john.id(), bob.id(), FamilyFile.Relation.Type.PARENT);
        Set<FamilyFile.Relation> relations = Set.of(johnIsParentOfBob);

        // When
        FamilyFile familyFile =new FamilyFile(name, members, relations);

        // Then
        assertSame(name, familyFile.name());
        assertSame(members, familyFile.members());
        assertSame(relations, familyFile.relations());
    }

    @Test
    public void shouldBeAbleToCreateMember() {
        // Given
        UUID id = UUID.randomUUID();
        String name = "John";
        FamilyFile.Member.Position position = new FamilyFile.Member.Position(0, 0);

        // When
        FamilyFile.Member john = new FamilyFile.Member(id, name, null, new FamilyFile.Member.Position(0, 0));

        // Then
        assertSame(id, john.id());
        assertSame(name, john.name());
        assertSame(null, john.picture());
        assertEquals(position, john.position());
    }

    @Test
    public void shouldBeAbleToCreatePosition() {
        // Given
        double x = 10;
        double y = 20;

        // When
        FamilyFile.Member.Position position = new FamilyFile.Member.Position(x, y);

        // Then
        assertEquals(x, position.x());
        assertEquals(y, position.y());
    }

    @Test
    public void shouldBeAbleToCreateRelation() {
        // Given
        FamilyFile.Member john = new FamilyFile.Member(UUID.randomUUID(), "John", null, new FamilyFile.Member.Position(0, 0));
        FamilyFile.Member bob = new FamilyFile.Member(UUID.randomUUID(), "Bob", null, new FamilyFile.Member.Position(0, 0));
        FamilyFile.Relation.Type type = FamilyFile.Relation.Type.PARENT;

        // When
        FamilyFile.Relation johnIsParentOfBob = new FamilyFile.Relation(john.id(), bob.id(), type);

        // Then
        assertSame(john.id(), johnIsParentOfBob.from());
        assertSame(bob.id(), johnIsParentOfBob.to());
        assertSame(type, johnIsParentOfBob.type());
    }
}
