package fr.iut.rodez.bubbles.service;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FamilyFileServiceTest {

    @Test
    void shouldReturnLoadedWhenFileIsCorrectlyLoaded() {
        // Given
        var file = new File("src/test/resources/family.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.Loaded loaded = assertInstanceOf(FamilyLoadingResult.Loaded.class, result);

        Family family = loaded.family();
        assertEquals("Doe", family.getName());

        Set<FamilyMember> members = family.getMembers();
        assertEquals(9, members.size());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsIncorrect() {
        // Given
        var file = new File("src/test/resources/incorrect-path.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        FileNotFoundException exception = assertInstanceOf(FileNotFoundException.class, loadingError.cause());
        // assertEquals("src/test/resources/incorrect-path.json (No such file or directory)", exception.getMessage());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsNotJson() {
        // Given
        var file = new File("src/test/resources/not-json.txt");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsNotValidJson() {
        // Given
        var file = new File("src/test/resources/invalid.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());
    }
}