package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.domain.exception.FamilyException;
import fr.iut.rodez.bubbles.domain.exception.MemberNotFoundException;
import fr.iut.rodez.bubbles.domain.exception.MembersAlreadyRelatedException;
import fr.iut.rodez.bubbles.domain.relation.FamilyRelation;
import fr.iut.rodez.bubbles.domain.relation.RelationType;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {

    @Test
    public void shouldBeAbleToCreateEmptyFamily() {
        // Given
        String name = "Boe";

        // When
        Family familyTest = new Family(name);

        // Then
        assertSame(name, familyTest.getName());
        assertTrue(familyTest.getMembers().isEmpty());
        assertTrue(familyTest.getRelations().isEmpty());
    }

    @Test
    public void shouldNotBeAbleToCreateFamilyWithInvalidParameters() {
        assertThrows(NullPointerException.class, () -> new Family(null));
        assertThrows(IllegalArgumentException.class, () -> new Family("  "));
        assertThrows(NullPointerException.class, () -> new Family(null, Set.of()));
        assertThrows(NullPointerException.class, () -> new Family("test", null));
    }

    @Test
    public void shouldBeAbleToCreateFamilyWithMembers() {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();

        String name = "Boe";
        Set<FamilyMember> members = Set.of(john, bob);

        // When
        Family familyTest = new Family(name, members);

        // Then
        assertSame(name, familyTest.getName());
        assertSame(members, familyTest.getMembers());
    }

    @Test
    public void shouldBeAbleToAddMember() {
        // Given
        Family family = new Family("test");
        FamilyMember john = FamilyMemberBuilder.invokeJohn();

        // When
        family.addMember(john);

        // Then
        assertEquals(1, family.getMembers().size());
        assertTrue(family.getMembers().contains(john));
    }

    @Test
    public void shouldBeAbleToRemoveMember() throws FamilyException {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        FamilyMember alice = FamilyMemberBuilder.invokeAlice();

        HashSet<FamilyMember> members = new HashSet<>() {{
                add(john);
                add(bob);
                add(alice);
        }};

        Family family = new Family("test", members);

        FamilyRelation johnRelationWithBob = new FamilyRelation(john.identity.id(), bob.identity.id(), RelationType.PARENT);
        FamilyRelation johnRelationWithAlice = new FamilyRelation(john.identity.id(), alice.identity.id(), RelationType.PARENT);
        FamilyRelation bobRelationWithAlice = new FamilyRelation(bob.identity.id(), alice.identity.id(), RelationType.PARENT);

        family.addRelation(johnRelationWithBob);
        family.addRelation(johnRelationWithAlice);
        family.addRelation(bobRelationWithAlice);

        // When
        family.removeMember(john.identity.id());

        // Then
        assertEquals(1, family.getRelations().size());
        assertTrue(family.getRelations().contains(bobRelationWithAlice));
    }

    @Test
    public void shouldBeAbleToAddRelation() throws FamilyException {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        FamilyMember alice = FamilyMemberBuilder.invokeAlice();
        Family family = new Family("test", Set.of(john, bob, alice));

        FamilyRelation johnRelationWithBob = new FamilyRelation(john.identity.id(), bob.identity.id(), RelationType.PARENT);
        FamilyRelation johnRelationWithAlice = new FamilyRelation(john.identity.id(), alice.identity.id(), RelationType.PARENT);

        // When
        family.addRelation(johnRelationWithBob);
        family.addRelation(johnRelationWithAlice);

        // Then
        assertEquals(2, family.getRelations().size());
        assertTrue(family.getRelations().contains(johnRelationWithBob));
        assertTrue(family.getRelations().contains(johnRelationWithAlice));
    }

    @Test
    public void shouldNotBeAbleToAddRelationWithSameMember() throws FamilyException {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        Family family = new Family("test", Set.of(john));

        FamilyRelation johnRelationWithJohn = new FamilyRelation(john.identity.id(), john.identity.id(), RelationType.PARENT);

        // When & Then
        assertThrows(FamilyException.class, () -> family.addRelation(johnRelationWithJohn));
    }


    @Test
    public void whenAddRelationButFromMemberNotFoundInFamilyShouldThrowException() {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        Family family = new Family("test", Set.of(bob));

        FamilyRelation relation = new FamilyRelation(john.identity.id(), bob.identity.id(), RelationType.PARENT);

        // When & Then
        assertThrows(MemberNotFoundException.class, () -> family.addRelation(relation));
    }

    @Test
    public void whenAddRelationButToMemberNotFoundInFamilyShouldThrowException() {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        Family family = new Family("test", Set.of(john));

        FamilyRelation relation = new FamilyRelation(john.identity.id(), bob.identity.id(), RelationType.PARENT);

        // When & Then
        assertThrows(MemberNotFoundException.class, () -> family.addRelation(relation));
    }

    @Test
    public void whenAddRelationButMembersAlreadyRelatedShouldThrowException() throws FamilyException {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        Family family = new Family("test", Set.of(john, bob));

        family.addRelation(new FamilyRelation(john.identity.id(), bob.identity.id(), RelationType.PARENT));

        // When & Then
        assertThrows(MembersAlreadyRelatedException.class,
                () -> family.addRelation(new FamilyRelation(bob.identity.id(), john.identity.id(), RelationType.PARENT)));
    }

    @Test
    public void shouldBeAbleToRemoveRelation() throws FamilyException {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        FamilyMember alice = FamilyMemberBuilder.invokeAlice();
        Family family = new Family("test", Set.of(john, bob, alice));

        FamilyRelation johnRelationWithBob = new FamilyRelation(john.identity.id(), bob.identity.id(), RelationType.PARENT);
        FamilyRelation johnRelationWithAlice = new FamilyRelation(john.identity.id(), alice.identity.id(), RelationType.PARENT);

        family.addRelation(johnRelationWithBob);
        family.addRelation(johnRelationWithAlice);

        // When
        family.removeRelation(johnRelationWithBob);

        // Then
        assertEquals(1, family.getRelations().size());
        assertTrue(family.getRelations().contains(johnRelationWithAlice));
    }

    @Test
    public void shouldBeAbleToFindRelationBetweenTwoMembers() throws FamilyException {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        FamilyMember alice = FamilyMemberBuilder.invokeAlice();
        Family family = new Family("test", Set.of(john, bob, alice));

        FamilyRelation johnRelationWithBob = new FamilyRelation(john.identity.id(), bob.identity.id(), RelationType.PARENT);
        FamilyRelation johnRelationWithAlice = new FamilyRelation(john.identity.id(), alice.identity.id(), RelationType.PARENT);

        family.addRelation(johnRelationWithBob);
        family.addRelation(johnRelationWithAlice);

        // When
        Optional<FamilyRelation> foundRelation = family.findRelationBetween(john.identity.id(), alice.identity.id());

        // Then
        assertSame(johnRelationWithAlice, foundRelation.get());
    }

    @Test
    public void shouldBeAbleToFindMember() {
        FamilyMember john = FamilyMemberBuilder.invokeJohn();
        FamilyMember bob = FamilyMemberBuilder.invokeBob();
        Family family = new Family("test", Set.of(john));

        // When
        Optional<FamilyMember> foundJohn = family.findMemberById(john.identity.id());
        Optional<FamilyMember> foundBob = family.findMemberById(bob.identity.id());

        // Then
        assertTrue(foundJohn.isPresent());
        assertFalse(foundBob.isPresent());
    }
}
