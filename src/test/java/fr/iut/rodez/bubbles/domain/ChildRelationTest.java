package fr.iut.rodez.bubbles.domain;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class ChildRelationTest {

    @Test
    public void shouldBeAbleToCreateChildRelation() {
        // Given
        UUID id = UUID.randomUUID();
        String name = "John";
        Identity identity = new Identity(id, name, null);
        Position position = new Position(0, 0);
        FamilyMember john = new FamilyMember(identity, position);

        // When
        ChildRelation childRelation = new ChildRelation(john);

        // Then
        assertSame(john, childRelation.related());
    }
}
