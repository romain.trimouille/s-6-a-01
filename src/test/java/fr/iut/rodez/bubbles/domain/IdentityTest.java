package fr.iut.rodez.bubbles.domain;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class IdentityTest {

    @Test
    public void shouldBeAbleToCreateIdentity() {
        // Given
        UUID id = UUID.randomUUID();
        String name = "John";

        // When
        Identity identity = new Identity(id, name, null);

        // Then
        assertSame(id, identity.id());
        assertSame(name, identity.name());
        assertSame(null, identity.picture());
    }
}
