package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;

import java.util.UUID;

public class FamilyMemberBuilder {

    public static FamilyMember invokeJohn() {
        UUID id = UUID.randomUUID();
        String name = "John";
        Identity identity = new Identity(id, name, null);
        Position position = new Position(0, 0);

        return new FamilyMember(identity, position);
    }

    public static FamilyMember invokeBob() {
        UUID id = UUID.randomUUID();
        String name = "Bob";
        Identity identity = new Identity(id, name, null);
        Position position = new Position(0, 0);

        return new FamilyMember(identity, position);
    }

    public static FamilyMember invokeAlice() {
        UUID id = UUID.randomUUID();
        String name = "Alice";
        Identity identity = new Identity(id, name, null);
        Position position = new Position(0, 0);

        return new FamilyMember(identity, position);
    }
}
