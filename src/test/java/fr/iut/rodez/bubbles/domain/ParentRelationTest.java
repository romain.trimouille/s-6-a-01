package fr.iut.rodez.bubbles.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParentRelationTest {

    @Test
    public void shouldBeAbleToCreateParentRelation() {
        // Given
        FamilyMember john = FamilyMemberBuilder.invokeJohn();

        // When
        ParentRelation parentRelation = new ParentRelation(john);

        // Then
        assertSame(john, parentRelation.related());
    }
}
