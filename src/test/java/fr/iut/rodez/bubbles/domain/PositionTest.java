package fr.iut.rodez.bubbles.domain;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class PositionTest {

    @Test
    public void shouldBeAbleToCreatePosition() {
        // Given
        double x = 0.0;
        double y = 10.6;

        // When
        Position position = new Position(x, y);

        // Then
        assertEquals(x, position.x());
        assertEquals(y, position.y());
    }

    @Test
    public void whenNegativeCoordinatesShouldThrowIllegalArgumentException() {
        // Given
        double positive = 0.0;
        double negative = -1.5;

        // Then
        var exceptionNegativeX = assertThrows(IllegalArgumentException.class, () -> new Position(negative, positive));
        assertEquals("X coordinate must be positive.", exceptionNegativeX.getMessage());

        var exceptionNegativeY = assertThrows(IllegalArgumentException.class, () -> new Position(positive, negative));
        assertEquals("Y coordinate must be positive.", exceptionNegativeY.getMessage());
    }

    @Test
    public void whenDistanceToAPositionShouldReturnDistanceBetweenBoth() {
        // Given
        Position aPosition = new Position(0,0);
        Position bPosition = new Position(2,3);
        Position cPosition = new Position(5, 10);

        // When
        double resultAToB = aPosition.distanceTo(bPosition);
        double resultAToC = aPosition.distanceTo(cPosition);
        double resultBToC = bPosition.distanceTo(cPosition);

        // Then
        assertEquals(Math.sqrt(13), resultAToB);
        assertEquals(Math.sqrt(125), resultAToC);
        assertEquals(Math.sqrt(58), resultBToC);
    }
}
