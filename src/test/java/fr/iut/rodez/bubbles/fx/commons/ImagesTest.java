package fr.iut.rodez.bubbles.fx.commons;

import javafx.scene.image.Image;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javafx.scene.image.Image;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ImagesTest {

    @Test
    public void testImagesPerson_givenWhenThen() {
        // GIVEN the Images class is loaded

        // WHEN the PERSON image is retrieved
        Image personImage = Images.PERSON;

        // THEN the PERSON image should not be null
        assertNotNull(personImage, "The PERSON image should not be null");
    }

    @Test
    public void testImagesRelation_givenWhenThen() {
        // GIVEN the Images class is loaded

        // WHEN the RELATION image is retrieved
        Image relationImage = Images.RELATION;

        // THEN the RELATION image should not be null
        assertNotNull(relationImage, "The RELATION image should not be null");
    }
}
