package fr.iut.rodez.bubbles.fx.commons;

import javafx.scene.paint.Paint;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javafx.scene.paint.Paint;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class PaintsTest {

    @Test
    public void testPaintsGlass_givenWhenThen() {
        // GIVEN the Paints class is loaded

        // WHEN the GLASS paint is retrieved
        Paint glassPaint = Paints.GLASS;

        // THEN the GLASS paint should not be null
        assertNotNull(glassPaint, "The GLASS paint should not be null");
    }

    @Test
    public void testPaintsBackground_givenWhenThen() {
        // GIVEN the Paints class is loaded

        // WHEN the BACKGROUND paint is retrieved
        Paint backgroundPaint = Paints.BACKGROUND;

        // THEN the BACKGROUND paint should not be null
        assertNotNull(backgroundPaint, "The BACKGROUND paint should not be null");
    }
}
