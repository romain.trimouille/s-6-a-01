package fr.iut.rodez.bubbles.fx.commons;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BackgroundsTest extends Application {

    @Override
    public void start(Stage primaryStage) {
        StackPane root = new StackPane();
        root.setBackground(Backgrounds.GLASS);
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Test
    public void testBackgroundsNotNull() {
        assertNotNull(Backgrounds.GLASS);
        assertNotNull(Backgrounds.TRANSPARENT);
    }
}
