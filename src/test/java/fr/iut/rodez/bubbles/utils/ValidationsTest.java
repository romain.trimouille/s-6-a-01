package fr.iut.rodez.bubbles.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static fr.iut.rodez.bubbles.utils.Validations.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationsTest {

    @Test
    void shouldThrowExceptionIfNull() {
        assertThrows(NullPointerException.class, () -> requireNotBlank(null));
        assertThrows(NullPointerException.class, () -> requireNotBlank(null, () -> "Value must not be null..."));
    }

    @Test
    void shouldReturnTheValueIfNotBlank() {
        String value = "test";

        assertEquals(value, requireNotBlank(value));
        assertEquals(value, requireNotBlank(value, () -> "Value must not be blank..."));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "\n", "\t"})
    void shouldThrowExceptionIfBlank(String value) {
        assertThrows(IllegalArgumentException.class, () -> requireNotBlank(value));
        assertThrows(IllegalArgumentException.class, () -> requireNotBlank(value, () -> "Value must not be blank..."));
    }


    @ParameterizedTest
    @ValueSource(doubles = {Double.POSITIVE_INFINITY, Double.MAX_VALUE, Double.MIN_VALUE, 1, 0.1})
    void shouldReturnTheValueIfItIsPositive(double value) {
        assertEquals(value, requirePositive(value));
        assertEquals(value, requirePositive(value, () -> "Value must be positive..."));
    }

    @ParameterizedTest
    @ValueSource(doubles = {Double.NaN, Double.NEGATIVE_INFINITY, Double.MAX_VALUE*-1, Double.MIN_VALUE*-1, -1, -0.1})
    void shouldThrowExceptionIfNumberIsNotPositive(double value) {
        assertThrows(IllegalArgumentException.class, () -> requirePositive(value));
        assertThrows(IllegalArgumentException.class, () -> requirePositive(value, () -> "Value must be positive..."));
    }

    @ParameterizedTest
    @ValueSource(ints = {Integer.MAX_VALUE, 1})
    void shouldReturnTheValueIfItIsStrictlyPositive(int value) {
        assertEquals(value, requireStrictlyPositive(value));
        assertEquals(value, requireStrictlyPositive(value, () -> "Value must be strictly positive"));
    }

    @ParameterizedTest
    @ValueSource(ints = {Integer.MIN_VALUE, 0, -1})
    void shouldThrowExceptionIfNumberIsNotStrictlyPositive(int value) {
        assertThrows(IllegalArgumentException.class, () -> requireStrictlyPositive(value));
        assertThrows(IllegalArgumentException.class, () -> requireStrictlyPositive(value, () -> "Value must be strictly positive"));
    }
}