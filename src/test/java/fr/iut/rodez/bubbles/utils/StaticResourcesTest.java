package fr.iut.rodez.bubbles.utils;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class StaticResourcesTest {

    @Test
    void testLoadResourceAsStream() {
        String resourcePath = "/testResource.txt";  // Adjust the path based on your resource location
        try (InputStream is = StaticResources.loadResourceAsStream(resourcePath)) {
            assertNotNull(is, "The input stream should not be null");
            assertTrue(is.available() > 0, "The input stream should have content");
        } catch (IOException e) {
            fail("IOException should not occur: " + e.getMessage());
        }
    }

    @Test
    void testLoadResourceAsStreamNotFound() {
        String resourcePath = "/nonExistentResource.txt";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            StaticResources.loadResourceAsStream(resourcePath);
        });
        assertEquals("Resource not found: " + resourcePath, exception.getMessage());
    }

    @Test
    void testRetrieveResourceURL() {
        String resourcePath = "/testResource.txt";  // Adjust the path based on your resource location
        URL url = StaticResources.retrieveResourceURL(resourcePath);
        assertNotNull(url, "The URL should not be null");
        assertTrue(url.toString().endsWith(resourcePath), "The URL should end with the resource path");
    }

    @Test
    void testRetrieveResourceURLNotFound() {
        String resourcePath = "/nonExistentResource.txt";
        URL url = StaticResources.retrieveResourceURL(resourcePath);
        assertNull(url, "The URL should be null for a non-existent resource");
    }
}
